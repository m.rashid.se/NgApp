import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";

import { AppComponent } from './app.component';
import { CoursesService } from './courses/courses.service'
import {CoursesComponent} from "./courses/courses.component";
import { ExamplesComponent } from './examples/examples.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
      CoursesComponent,
      ExamplesComponent,
      HomeComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'course',
        component: CoursesComponent
      },
      {
        path: 'example',
        component: ExamplesComponent
      },
    ])
  ],
  providers: [
    CoursesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
