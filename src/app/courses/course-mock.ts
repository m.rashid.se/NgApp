import {Course} from "./course";

export const COURSES:Course[] = [
    {id: 1,name: "Islamic Study", creditHours: 2, description: "This is an compulsory course."},
    {id: 2,name: "Pak Study", creditHours: 2, description: "This is an Optional course."},
    {id: 3,name: "English", creditHours: 3, description: "This is an Optional course."},
    {id: 4,name: "Urdu", creditHours: 3, description: "This is an Optional course."},
    {id: 5,name: "Physics", creditHours: 3, description: "This is an compulsory course."},
    {id: 6,name: "Bio", creditHours: 3, description: "This is an compulsory course."},
    {id: 7,name: "Chemistry", creditHours: 3, description: "This is an compulsory course."},
    {id: 8,name: "Computer", creditHours: 3, description: "This is an compulsory course."},
    {id: 9,name: "Math", creditHours: 3, description: "This is an compulsory course."},
    {id: 10,name: "Education", creditHours: 3, description: "This is an compulsory course."},
];