import {Component, OnInit} from "@angular/core";
import {CoursesService} from "./courses.service";
import {Course} from "./course";

@Component({
    selector: 'courses',
    templateUrl: './courses.component.html'
})

export class CoursesComponent implements OnInit{

    courses:Course[];
    showForm:boolean;

    constructor(private coursesService: CoursesService) {
        // this.courses.push();
    }

    ngOnInit() {
        // this.getCourses();
        this.coursesService.getCoursesByHttp().subscribe(courses => this.courses=courses);
        this.showForm = false;
    }

    getCourses():void{
        this.courses = this.coursesService.getCourses();
    }

    onSubmit(course):void{
        console.log(course);
    }

    onClickShowAddForm():void{
        this.showForm = true;
    }

    onClickShowList():void{
        this.showForm = false;
    }
}