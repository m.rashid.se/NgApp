import {Injectable} from "@angular/core";
import {Http, Response} from '@angular/http';
import { Course } from './course';
import { COURSES } from './course-mock';
import {Observable} from "rxjs";

@Injectable()
export class CoursesService{

    private _courseListUrl:string;
    constructor(private _http: Http) {
        this._courseListUrl = 'assets/data/courses.json';
    }

    getCourses(): Course[]{
        return COURSES;
    }

    getCoursesByHttp():Observable<Course[]>{
        return this._http.get(this._courseListUrl)
            .map((response: Response) => <Course[]>response.json())

            .do(data => console.log("Course data" + JSON.stringify(data)))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || 'Internal Server error');
    }
}