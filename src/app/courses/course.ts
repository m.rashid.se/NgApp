export interface Course {
    id:number
    name:string
    creditHours:number
    description:string
}
